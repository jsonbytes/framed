 # framed  
 Features:  
 - Drag and drop functionality
 - Multiple backgrounds to choose from  
 - Multiple frame sizes to use  
 - Wall size can be manipulated to be the same size as yours  
 - Allows deletion of frames  
 - Hover effect to show frame size once on wall
